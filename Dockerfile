FROM composer/composer

RUN apt-get update
RUN apt-get install -y autoconf pkg-config libssl-dev

RUN pecl install mongodb

RUN echo "extension=mongodb.so" >> /usr/local/etc/php/conf.d/mongodb.ini

COPY wait-for-mongo.sh /wait-for-mongo.sh

ENTRYPOINT ["/wait-for-mongo.sh"]
