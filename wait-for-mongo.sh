#!/bin/bash

until curl -v -s mongo:27017;
do
	echo "MongoDB isn't available."
	sleep 1
done

composer --ansi "$@"
